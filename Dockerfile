FROM kamerk22/laravel-alpine:7.2-mysql-nginx

COPY composer.json composer.json
COPY composer.lock composer.lock

RUN composer install --prefer-dist --no-scripts --no-autoloader && rm -rf /root/.composer

RUN apk update && \
    apk add libxml2-dev


RUN docker-php-ext-install soap

ADD conf/nginx/default.conf /etc/nginx/conf.d/

ADD . .

RUN cp .env.dev_server .env
RUN cp conf/supervisor/services.ini /etc/supervisor.d/

RUN chown -R www-data:www-data \
        /var/www/storage \
        /var/www/bootstrap/cache

RUN composer dump-autoload --no-scripts --optimize
RUN ln -s /var/www/storage/app/ /var/www/public/storage
RUN chmod 777 -R storage bootstrap/cache
